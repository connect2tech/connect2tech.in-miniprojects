package com.c2t.mailer.kashish;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MailerUtil {

	public static String covertFolderToZip(String fileName) {
		return null;
	}

	/**
	 * This method converts HTML file to String. It read the HTML file, trims
	 * each line that is read, and removes/ignores the spaces between lines of
	 * HTML file.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String covertHtmlToString(String fileName) {

		StringBuffer sbuf = new StringBuffer();

		try {

			String rootDir = System.getProperty("user.dir");
			rootDir = rootDir.replace('\\', '/');

			String fNamePath = rootDir + "/src/main/java/com/c2t/mailer/kashish/" + fileName;
			File f = new File(fNamePath);

			BufferedReader b = new BufferedReader(new FileReader(f));

			String readLine = "";

			while ((readLine = b.readLine()) != null) {

				// Covert the file into String.
				// Trim empty spaces.
				if (readLine != null && readLine.length() > 0) {
					sbuf.append(readLine.trim());
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(sbuf);

		return sbuf.toString();
	}

	public static void main(String[] args) throws IOException {
		// covertHtmlToString("First.html");
	}

}
