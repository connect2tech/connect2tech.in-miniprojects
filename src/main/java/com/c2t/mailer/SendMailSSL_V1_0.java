package com.c2t.mailer;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.c2t.util.MailerUtil;

public class SendMailSSL_V1_0 {
	static String email = "message4naresh@gmail.com";
	static String password = "APIAutomation12#$";
	static String subject = "API Automation Testing - Naresh Chaurasia";
	static Logger logger = Logger.getLogger(ReadFromExcel.class);
	static List<String> receipents = new ArrayList<String>();
	static int i = 0;

	public static void main(String[] args) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		try {
			receipents = ReadFromExcel.readEmailFromExcel();
			logger.debug("receipents=>" + receipents);

			for (i = 0; i < receipents.size(); i++) {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(email));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receipents.get(i)));
				message.setSubject(subject);

				message.setContent(MailerUtil.covertHtmlToString("API-Automation.html"), "text/html");

				Transport.send(message);

				System.out.println("Mail sent to =>" + receipents.get(i) + " and the count is =>" + i);
				logger.debug("Mail sent to =>" + receipents.get(i) + " and the count is =>" + i);

			}
		} catch (Exception e) {
			logger.debug("Mail failed for =>" + receipents.get(i));
			logger.debug("Error =>" + e.toString());
		}

	}
}
