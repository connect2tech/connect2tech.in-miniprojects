package com.c2t.mailer;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.c2t.util.MailerUtil;

public class SendMailSSL_Multiple {
	static String email = "naresh.javapro@gmail.com";
	static String password = "Hello12#$";

	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		try {

			InternetAddress[] recipientAddress = new InternetAddress[2];
			recipientAddress[0] = new InternetAddress(email);
			recipientAddress[1] = new InternetAddress("message4naresh@gmail.com");

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email));
			message.setRecipients(Message.RecipientType.TO, recipientAddress);
			message.setSubject("API Automation Testing");

			message.setContent(MailerUtil.covertHtmlToString("UrbanProMailTemplate.html"), "text/html");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
