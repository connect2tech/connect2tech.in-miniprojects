package com.c2t.mailer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	static Logger logger = Logger.getLogger(ReadFromExcel.class);
	static List<String> receipents = new ArrayList<String>();

	public static List<String> readEmailFromExcel() throws IOException {
		File f = new File("D:/nchaurasia/Automation-Architect/connect2tech.in-miniprojects/E-Mail.xlsx");
		InputStream is = new FileInputStream(f);
		XSSFWorkbook workbook = new XSSFWorkbook(is);
		XSSFSheet sheet = workbook.getSheet("Java Selenium Automation");
		Iterator<Row> rows = sheet.rowIterator();

		while (rows.hasNext()) {

			Row row = rows.next();

			Iterator<Cell> cells = row.cellIterator();

			while (cells.hasNext()) {
				Cell cell = cells.next();
				receipents.add(cell.toString());
				// logger.debug("cell=>" + cell);
			}

		}
		//logger.debug("receipents=>" + receipents);
		//System.out.println("Done...");
		return receipents;
	}

	public static void main(String[] args) throws IOException {

		// FileInputStream file = new FileInputStream(new
		// File("howtodoinjava_demo.xlsx"));
		// XSSFWorkbook workbook = new XSSFWorkbook(file);
		// XSSFSheet sheet = workbook.getSheetAt(0);
		// sheet.getSheetName().equals("Employee Data")
		// Iterator<Row> rowIterator = sheet.iterator();
		// Row row = rowIterator.next();
		// Iterator<Cell> cellIterator = row.cellIterator();

		readEmailFromExcel();

	}

}