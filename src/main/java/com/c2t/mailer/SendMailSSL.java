package com.c2t.mailer;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.c2t.util.MailerUtil;

public class SendMailSSL {
	static String email = "message4naresh@gmail.com";
	static String password = "APIAutomation12#$";
	static String subject = "API Automation Testing - Naresh Chaurasia";

	static List<String> receipents = new ArrayList<String>();

	public static void main(String[] args) {

		receipents.add("message4naresh@gmail.com");
		receipents.add("arunthampi@live.com");
		receipents.add("avayambalk@gmail.com");
		receipents.add("dipti.vasudevasingh21@gmail.com");
		receipents.add("gaston@stunn.co.uk");
		receipents.add("homzy.53@gmail.com");
		receipents.add("janaki.kola@gmail.com");
		receipents.add("nazmul619@gmail.com");
		receipents.add("pradeepbhadikar@yahoo.co.in");
		receipents.add("sunainasram@gmail.com");
		receipents.add("umahari@gmail.com");

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		for (int i = 0; i < receipents.size(); i++) {

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(email));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receipents.get(i)));
				message.setSubject(subject);

				message.setContent(MailerUtil.covertHtmlToString("UrbanProMailTemplate.html"), "text/html");

				Transport.send(message);

				System.out.println("Done");

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
